#t!/bin/bash

#MODULE IA : Intelligence Aléatoire pour Jarvis

js_ia_findLine(){

	sed -n '/^'$1'/=' $ia_route
}

js_ia_showLine(){

	line=$(sed -n "$1"p $ia_route)
	echo $line | sed 's/ /_/g'
}

js_ia_countCells(){

    echo $1 | awk -F';' '{print NF}'
}

js_ia_random(){

	min=2
	max=$1
	number=$[($RANDOM % ($[$max - $min] + 1)) + $min]
	echo $number
}

js_ia_loopCell(){

	line=$(js_ia_findLine $1)
    if [ -z $line ]
    then
        echo "false"
    else
        group=$(js_ia_showLine $line)
        group=$(echo $group | sed 's/_/ /g')
        nameGroup=$(echo $group | awk -F';' '{print $1}')
		itemsGroup=$(echo $group | cut -d';' -f2-)
		echo "$itemsGroup"
    fi
}

js_ia_getCell(){

	echo $1 | awk -F';' '{print $'$2'}'
}

js_ia_addCell(){

	line=$(js_ia_findLine $2)
	if [ -z $line ]
    then
		js_ia_say "erreur"
        say "je n'ai pas trouvé $2 dans ma mémoire"
    else
        sed -i $line's/$/'";${1}"'/' $ia_route
    fi
}

js_ia_upCell(){

    line=$(js_ia_findLine $3)
    if [ -z $line ]
    then
        js_ia_say "erreur"
        say "je n'ai pas trouvé $3 dans ma mémoire"
    else
        sed -i $line's/'";${1}"'/;'$2'/' $ia_route
    fi
}

js_ia_delCell(){

	line=$(js_ia_findLine $2)
	if [ -z $line ]
    then
		js_ia_say "erreur"
        say "je n'ai pas trouvé $2 dans ma mémoire"
    else
		sed -i $line's/'";${1}"'//' $ia_route
	fi
}

js_ia_getGroup(){

    line=$(js_ia_findLine $1)
    if [ -z $line ]
    then
        js_ia_say "erreur"
        say "je n'ai pas trouvé $1 dans ma mémoire"
    else
        group=$(js_ia_showLine $line)
        group=$(echo $group | sed 's/_/ /g')
        nameGroup=$(echo $group | awk -F';' '{print $1}')
		itemsGroup=$(echo $group | cut -d';' -f2-)
		itemsGroup=$(echo $itemsGroup | sed 's/;/, /g')
		say "dans le groupe $nameGroup il y a"
		say "$itemsGroup"
    fi
}

js_ia_getAllGroups(){

	say "voici les groupes"
	groups=$(awk -F';' '{print $1}' $ia_route)
	groups=$(echo $groups | tr -d "\n")
	groups=$(echo $groups | sed 's/ /, /g')
	say "$groups"
}

js_ia_addGroup(){

	line=$(js_ia_findLine $1)
    if [ -z $line ]
    then
		echo "$1" >> $ia_route
    else
		js_ia_say "erreur"
        say "Le groupe $1 existe déja"
    fi
}

js_ia_delGroup(){

    line=$(js_ia_findLine $1)
	if [ -z $line ]
    then
		js_ia_say "erreur"
        say "je n'ai pas trouvé $1 dans ma mémoire"
    else
        sed -i $line'd' $ia_route
    fi
	js_ia_delEmptyLines
}

js_ia_delAllLines(){

	echo "" > $ia_route
	js_ia_delEmptyLines
}

js_ia_delEmptyLines(){

    sed -i '/^$/d' $ia_route
}

js_ia_showBrain(){

	sed '' $ia_route
}

js_ia_getText(){

    line=$(js_ia_findLine $1)
    line=$(js_ia_showLine $line)
    nbCells=$(js_ia_countCells $line)
    nbRandom=$(js_ia_random $nbCells)
    cell=$(js_ia_getCell $line $nbRandom)
    echo $cell | sed 's/_/ /g'
}

js_ia_say(){

	toSay=$(js_ia_getText $1)
	say "$toSay $2"
}

js_ia_getStrictText(){

    line=$(js_ia_findLine $1)
    line=$(js_ia_showLine $line)
    cell=$(js_ia_getCell $line $2)
    echo $cell | sed 's/_/ /g'
}

js_ia_sayStrict(){

    toSay=$(js_ia_getStrictText $1 $2)
    say "$toSay $3"
}

#necessite jarvis-messenger
js_ia_sendToMe(){

	jv_pg_fb_send_message "$var_jv_pg_fb_main_receiver_id" "$var_jv_pg_fb_main_receiver_name" "$1" "$2"
}
