## Description
Non ce plugin n'est pas une intelligence artificielle mais une intelligence aléatoire.
Son fonctionnement est simple, apprenner à Jarvis des synonymes de mots ou de phrases et en fonction de l'ordre que vous allez lui adresser il choisira au hasard une réponse.

De cette manières il répondera différament à la même question s'il elle est posé plusieurs fois de suite.

un fichier brain.csv est créer automatiquement c'est lui qui contient tout ce que Jarvis apprend.

## Structure brain.csv:
```
GROUPE1;SYNONYME;SYNONYME;SYNONYME
GROUPE2;SYNONYME;SYNONYME
GROUPE3;SYNONYME;SYNONYME;SYNONYME;SYNONYME
```
La premiere colonne contient la mot clef du groupe synonymes, il doit être passé en argument dans la fonction, c'est grace à lui que Jarvis trouvera la ligne correspondande au mot clefs et de ce fait à l'ensemble des synonymes.
La selection aléatoire se fait sur toutes les colonnes sauf la 1ere (Possibilité de le modifié dans la fonction js_ia_random() en remplacant min=2 par par min=1).

En appelant la fonction et en passant salutation comme argument il choissira au hasard parmis "bonjour, hello, salut, coucou".

Nouveau : Possibilité d'afficher la colonne voulu d'un groupe en spéficiant le numéro de la colonne. (Test strict)

## Commandes
```
*DONNE*GROUP* (*)==js_ia_getGroup (1)
*DONNE*TOU*GROUP*==js_ia_getAllGroups
*AJOUTE* (*) a (*)==js_ia_addCell (1) (2)
*SUPPRIME* (*) de (*)==js_ia_delCell (1) (2)
*AJOUT*GROUP* (*)==js_ia_addGroup (1)
*SUPPRIME*GROUP* (*)==js_ia_delGroup (1)
*EFFACE*VIDE*==js_ia_delEmptyLines
*EFFACE*MEMOIR*==js_ia_delAllLines

*TEST*IA*==js_ia_say salutation $username && js_ia_say question
*TEST*STRICT*==js_ia_getStrictText groupe NumeroColonne
```

## Usage
Pour tester un peu la fonction aléatoire...
```
You: test ia
Jarvis: bonjour jérémy comment ca va
You: test ia
Jarvis: hello jérémy tu va bien
You: test ia
Jarvis: coucou jérémy ca va
```
Pour lister les synonymes d'un groupe
```
You: Donne moi le groupe salutation
Jarvis: c'est fait
```
Pour lister l'ensemble des groupes
```
You: Donne moi tous les groupes
Jarvis: ayé
```
Pour ajouter un synonyme à un groupe
```
You: Ajoute bonjour a salutation
Jarvis: ayé
```
Pour supprimer un synonyme d'un groupe
```
You: Supprime hello de salutation
Jarvis: c'est fait
```
Pour ajouter un groupe
```
You: Ajoute le groupe negation
Jarvis: c'est fait
```
Pour supprimer un groupe (supprimera également l'ensemble des synonyme y apartenant)
```
You: Supprime le groupe confirmation
Jarvis: c'est fait
```
Pour effacer les lignes vide du fichier (se fait automatiquement)
```
You: Efface les lignes vides
Jarvis: ayé
```
Pour supprimer l'ensemble du contenu du fichier brain.csv
```
You: Efface ta mémoire
Jarvis: C'est fait
```

## Author
[Jérémy GAUDIN](http://gaudinjeremy.free.fr)
